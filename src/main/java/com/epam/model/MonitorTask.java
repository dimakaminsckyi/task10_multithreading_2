package com.epam.model;

import com.epam.model.lock.LockCustom;
import com.epam.model.lock.ReentrantLockCustom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MonitorTask {

    private static Logger log = LogManager.getLogger(MonitorTask.class);
    private LockCustom lock = new ReentrantLockCustom();

    public void doTask() {
        ExecutorService executor = Executors.newFixedThreadPool(3);
        executor.execute(this::printFirst);
        executor.execute(this::printFirst);
        executor.execute(this::printFirst);
        executor.shutdown();
    }

    private void printFirst() {
        lock.lock();
        log.info("Lock...");
        try {
            Thread.sleep(2000);
            log.info("First");
            printSecond();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void printSecond() {
        try {
            Thread.sleep(2000);
            log.info("Second");
            printThird();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void printThird() {
        try {
            Thread.sleep(2000);
            log.info("Third");
            lock.unlock();
            log.info("Unlock...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
