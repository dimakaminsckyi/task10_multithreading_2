package com.epam.controller;

public interface MainController {

    void runMonitorTask();

    void runQueueTask();
}
