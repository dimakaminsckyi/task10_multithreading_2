package com.epam.controller;

import com.epam.model.MonitorTask;
import com.epam.model.BlockingQueueTask;

public class MainControllerImpl implements MainController {

    @Override
    public void runMonitorTask() {
        new MonitorTask().doTask();
    }

    @Override
    public void runQueueTask() {
        new BlockingQueueTask().doTask();
    }
}
