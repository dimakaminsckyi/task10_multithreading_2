package com.epam.view;

import com.epam.controller.MainController;
import com.epam.controller.MainControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {

    private static Logger log = LogManager.getLogger(MainView.class);
    private MainController controller = new MainControllerImpl();
    private Map<String, String> menu;
    private Map<String, Runnable> methodRun;

    public MainView() {
        initMenu();
        methodRun = new LinkedHashMap<>();
        methodRun.put("monitor", () -> controller.runMonitorTask());
        methodRun.put("queue", () -> controller.runQueueTask());
        methodRun.put("exit", () -> log.info("Exit"));
    }

    public void runMenu() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder flagExit = new StringBuilder("");
        do {
            menu.values().forEach(log::info);
            try {
                flagExit.setLength(0);
                flagExit.append(scanner.nextLine());
                methodRun.get(flagExit.toString()).run();
            } catch (NullPointerException e) {
                log.warn("Input correct option");
            }
        } while (!flagExit.toString().equals("exit"));
    }

    private void initMenu(){
        menu = new LinkedHashMap<>();
        menu.put("monitor", "monitor - Run monitor Task");
        menu.put("queue", "queue - Run BlockingQueue task");
        menu.put("exit", "exit - Exit");
    }
}

